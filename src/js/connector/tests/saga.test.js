import * as mockActions from '../../../testUtils/mockData/mockActions';
import connectorSaga, * as saga from '../saga';
import { takeEvery } from 'redux-saga/effects';

describe('Connector saga', () => {
    let sandbox = null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    after(() => {
        sandbox.restore();
    });

    describe('connectorSaga', () => {
        const generator = connectorSaga();

        it('should takeEvery FETCH_CURRENT_WEATHER', () => {
            assert.deepEqual(
                generator.next().value,
                takeEvery(mockActions.getCurrentWeather().type, saga.getCurrentWeather)
            );
        });

        it('should be finished', () => {
            assert.isTrue(generator.next().done);
        });
    });
});