import axios from 'axios';
import config from '../config/config';
import { eventChannel } from 'redux-saga';
import { put, call, take, takeEvery, select } from 'redux-saga/effects';
import ActionTypes from '../constants/actionTypes';
import * as actions from './actions';
import * as selectors from './selectors';

export default function* connectorSaga() {
    yield takeEvery(ActionTypes.FETCH_CURRENT_WEATHER, getCurrentWeather);
}

export function request(params) {
    return eventChannel(emitter => {
        const { method, url } = params.requestParams;

        axios[method](`${config.openWeatherAPI.baseURL}${url}`)
            .then(res => {
                emitter({
                    type: `${params.target}_success`,
                    payload: res.data,
                });
            })
            .catch(err => {
                emitter({
                    type: `${params.target}_failure`,
                    payload: err.data,
                });
            });
    });
}

export function* sendRequest(action) {
    if (!action || !action.payload || !action.payload.target || !action.payload.requestParams) {
        return false;
    }

    const channel = yield call(request, action.payload);
    const response = yield take(channel);

    yield put(response);
    channel.close();
}

export function* getCurrentWeather(action) {
    const actionSendRequest = yield call(actions.sendRequest, {
        target: action.type,
        requestParams: {
            method: 'get',
            url: config.openWeatherAPI.currentWeather,
        },
    });

    yield call(sendRequest, actionSendRequest);
}