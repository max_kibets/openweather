'use strict';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { compose, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../roots/rootReducer';
import rootSaga from '../roots/rootSaga';
import config from '../config/config';
import App from '../modules/App/';

document.addEventListener('DOMContentLoaded', () => {
    init();
});

function init() {
    document.title = config.title;
    // setFavIcon(configCore.favIcon);

    const initialState = {
        temperatureUnit: config.toggle.temperatureUnit.inputs[0],
    };

    const sagaMiddleware = createSagaMiddleware({
        onError: error => {
            alert('Critical error acquired! See console for more details.');
            console.error(error);
            sagaMiddleware.run(rootSaga);
        },
    });

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(sagaMiddleware)),
    );
    
    sagaMiddleware.run(rootSaga);

    render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById('root')
    );
}

export function setFavIcon(src) {
    if (!src) {
        return false;
    }

    const favicon = document.createElement('link');

    favicon.rel = 'icon';
    favicon.href = src;
    document.head.appendChild(favicon);
}
