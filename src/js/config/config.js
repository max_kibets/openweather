export default {
    title: 'Open Weather',
    toggle: {
        temperatureUnit: {
            name: 'temperatureUnit',
            inputs: ['metric', 'imperial'],
        },
    },
    openWeatherAPI: {
        key: 'dfb8d31ef4af71a6038f808ccf6f9380',
        baseURL: 'api.openweathermap.org/data/2.5/',
        currentWeather: 'weather',
        forecast: 'forecast',
    },
};
