import styled from 'styled-components';

export const Wrap = styled.header`
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    margin-bottom: 15px;
`;