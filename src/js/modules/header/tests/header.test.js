import React from 'react';
import Component from '../Header.jsx';

describe('Header component', () => {
    it('snapshot should render correctly', () => {
        const props = {
            currentUnit: 'metric',
            toggleTemperatureUnit: () => {},
        };

        const component = shallow(
            <Component { ...props } />
        );
        expect(component).matchSnapshot();
    });
});