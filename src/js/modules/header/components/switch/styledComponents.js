import styled from 'styled-components';

export const Toggle = styled.div`
    display: flex;
    border: 1px solid #000;
    padding: 1px 2px;
    border-radius: 13px;
`;

const label = styled.label`
    border: 1px solid transparent;
    padding: 0 5px;
    min-width: 20px;
    text-align: center;
    cursor: pointer;
    transition: all .25s ease;

    ${props => props.active && 
        'border-color: #000;' +
        'color: #fff;' +
        'background-color: green;' + 
        'cursor: initial;'}

    ${props => props.first && 
        'border-radius: 11px 0 0 11px;'
    }

    ${props => props.last && 
        'border-radius: 0 11px 11px 0;'
    }
`;

const radioInput = styled.input.attrs(props => ({
    type: 'radio',
}))`
    display: none;
`;

Toggle.Label = label;
Toggle.RadioInput = radioInput;