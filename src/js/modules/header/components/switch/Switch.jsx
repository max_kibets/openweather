import React from 'react';
import PropTypes from 'prop-types';
import strings from '../../../../strings/en.js';
import { Toggle } from './styledComponents.js';

const Switch = ({ inputs, name, activeInput, handlerClick }) => {
    const toggleInputs = inputs.map((input, i, arr) => {
        const attr = {
            active: activeInput === input,
            first: i === 0,
            last: i === arr.length - 1,
        };

        return (
            <Toggle.Label
                key={ input }
                { ...attr }>
                { strings.toggle.temperatureUnit[input] }
                <Toggle.RadioInput 
                    name={ name }
                    value={ input }
                    onClick={ event => handlerClick(event.target.value) }/>
            </Toggle.Label>
        );
    });

    return (
        <Toggle>
            { toggleInputs }
        </Toggle>
    );
};

Switch.propTypes = {
    inputs: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    activeInput: PropTypes.string.isRequired,
    handlerClick: PropTypes.func.isRequired,
};

export default React.memo(Switch);