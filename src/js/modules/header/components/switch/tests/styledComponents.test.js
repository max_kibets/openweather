import { Toggle } from '../styledComponents.js';

describe('Switch styled components', () => {
    it('Toggle should have correct styles', () => {
        const component = shallowRender(<Toggle/>);

        expect(component).toHaveStyleRule('display', 'flex');
        expect(component).toHaveStyleRule('border', '1px solid #000');
        expect(component).toHaveStyleRule('padding', '1px 2px');
        expect(component).toHaveStyleRule('border-radius', '13px');
    });

    it('Toggle.Label should have correct styles', () => {
        const props = {
            active: false,
            first: false,
            last: false,
        };

        const component = shallowRender(<Toggle.Label { ...props }/>);

        expect(component).toHaveStyleRule('border', '1px solid transparent');
        expect(component).toHaveStyleRule('padding', '0 5px');
        expect(component).toHaveStyleRule('min-width', '20px');
        expect(component).toHaveStyleRule('text-align', 'center');
        expect(component).toHaveStyleRule('cursor', 'pointer');
        expect(component).toHaveStyleRule('transition', 'all .25s ease');
    });

    it('Toggle.Label should have correct styles', () => {
        const props = {
            active: true,
            first: true,
            last: false,
        };

        const component = shallowRender(<Toggle.Label { ...props }/>);

        expect(component).toHaveStyleRule('border', '1px solid transparent');
        expect(component).toHaveStyleRule('padding', '0 5px');
        expect(component).toHaveStyleRule('min-width', '20px');
        expect(component).toHaveStyleRule('text-align', 'center');
        expect(component).toHaveStyleRule('transition', 'all .25s ease');
        expect(component).toHaveStyleRule('border-color', '#000');
        expect(component).toHaveStyleRule('color', '#fff');
        expect(component).toHaveStyleRule('background-color', 'green');
        expect(component).toHaveStyleRule('cursor', 'initial');
        expect(component).toHaveStyleRule('border-radius', '11px 0 0 11px');
    });

    it('Toggle.Label should have correct styles', () => {
        const props = {
            active: true,
            first: false,
            last: true,
        };

        const component = shallowRender(<Toggle.Label { ...props }/>);

        expect(component).toHaveStyleRule('border', '1px solid transparent');
        expect(component).toHaveStyleRule('padding', '0 5px');
        expect(component).toHaveStyleRule('min-width', '20px');
        expect(component).toHaveStyleRule('text-align', 'center');
        expect(component).toHaveStyleRule('transition', 'all .25s ease');
        expect(component).toHaveStyleRule('border-color', '#000');
        expect(component).toHaveStyleRule('color', '#fff');
        expect(component).toHaveStyleRule('background-color', 'green');
        expect(component).toHaveStyleRule('cursor', 'initial');
        expect(component).toHaveStyleRule('border-radius', '0 11px 11px 0');
    });

    it('Toggle.RadioInput should have correct styles', () => {
        const component = shallowRender(<Toggle.RadioInput/>);

        expect(component).toHaveStyleRule('display', 'none');
    });
});