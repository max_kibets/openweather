import React from 'react';
import Component from '../Switch.jsx';

describe('Switch component', () => {
    it('snapshot should render correctly', () => {
        const props = {
            inputs: [],
            name: '',
            activeInput: '',
            handlerClick: () => {},
        };

        const component = shallow(
            <Component { ...props } />
        );
        expect(component).matchSnapshot();
    });
});