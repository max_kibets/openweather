import React from 'react';
import PropTypes from 'prop-types';
import config from '../../config/config.js';
import { Wrap } from './styledComponents.js';
import Switch from './components/switch/';

const Header = ({ currentUnit, toggleTemperatureUnit }) => {
    return (
        <Wrap>
            <Switch 
                { ...config.toggle.temperatureUnit }
                activeInput={ currentUnit }
                handlerClick={ toggleTemperatureUnit }/>
        </Wrap>
    );
};

Header.propTypes = {
    currentUnit: PropTypes.string.isRequired,
    toggleTemperatureUnit: PropTypes.func.isRequired,
};

export default React.memo(Header);