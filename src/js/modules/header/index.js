import Component from './Header.jsx';
import { connect } from 'react-redux';
import * as actions from './actions.js';
import * as selectors from './selectors.js';

export const mapStateToProps = state => ({
    currentUnit: selectors.getCurrentUnit(state),
});

export const mapDispatchToProps = dispatch => ({
    toggleTemperatureUnit: payload => dispatch(actions.toggleTemperatureUnit(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);