import Component from './App.jsx';
import { connect } from 'react-redux';
// import * as actions from './actions';
// import * as selectors from './selectors';

export const mapStateToProps = state => ({
    // test: selectors.testSelector(state),
});

export const mapDispatchToProps = dispatch => ({
    // testAction: () => dispatch(actions.testAction()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);