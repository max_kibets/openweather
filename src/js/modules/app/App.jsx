import React from 'react';
import PureComponent from '../../base/PureComponent.jsx';
import ErrorBoundary from '../../decorators/ErrorBoundary.jsx';
import { ResetCSS, Layout } from './styledComponents.js';
import Search from '../search/';
import Header from '../header/';

@ErrorBoundary
export default class App extends PureComponent {
    render() {
        return (
            <React.Fragment>
                <ResetCSS/>
                <Layout>
                    <Header/>
                    <Search/>
                </Layout>
            </React.Fragment>
        );
    }
};