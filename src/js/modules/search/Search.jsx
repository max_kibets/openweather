import React from 'react';
import strings from '../../strings/en.js';
import { Form, Input, Button } from './styledComponents.js';

const Search = () => {
    return (
        <Form>
            <Input placeholder={ strings.inputs.searchPlaceholder }/>
            <Button indentLeft={ true }>
                { strings.buttons.search }
            </Button>
        </Form>
    );
};

export default React.memo(Search);