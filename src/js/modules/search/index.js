import Component from './Search.jsx';
import { connect } from 'react-redux';

export const mapStateToProps = state => ({
});

export const mapDispatchToProps = dispatch => ({
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);