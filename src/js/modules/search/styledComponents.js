import styled from 'styled-components';

export const Form = styled.form`
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
    max-width: 400px;
`;

export const Input = styled.input.attrs(props => ({
    type: props.inputType || 'text',
}))`
    border: 1px solid #000;
    padding: 5px 10px;
    width: 100%;
`;

export const Button = styled.button`
    border: 1px solid #000;
    margin-left: ${props => props.indentLeft ? '10px' : '0'};
    padding: 5px 15px;
    background-color: transparent;
    cursor: pointer;
    transition: all .25s ease;

    &:hover,
    &:focus {
        background-color: green;
        color: #fff;
    }
`;