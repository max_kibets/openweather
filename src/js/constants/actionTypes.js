export default Object.defineProperties({}, {
    TOGGLE_TEMPERATUTE_UNIT: { value: 'toggle_temerature_unit', writable: false },

    FETCH_CURRENT_WEATHER: { value: 'fetch_current_weather', writable: false },
    FETCH_CURRENT_WEATHER_SUCCESS: { value: 'fetch_current_weather_success', writable: false },
    FETCH_CURRENT_WEATHER_FAILURE: { value: 'fetch_current_weather_failure', writable: false },
});
