import ActionTypes from '../../constants/actionTypes';

export default (state = '', action) => {
    switch (action.type) {
        case ActionTypes.TOGGLE_TEMPERATUTE_UNIT:
            return action.payload;

        default: 
            return state;
    }
};