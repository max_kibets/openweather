export default {
    inputs: {
        searchPlaceholder: 'Your city name',
    },
    buttons: {
        search: 'Search',
    },
    toggle: {
        temperatureUnit: {
            metric: '°C',
            imperial: '°F',
        },
    },
};
