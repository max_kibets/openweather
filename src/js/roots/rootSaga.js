import {
    all,
    call,
} from 'redux-saga/effects';

const sagasList = [
];

export default function* watchRootSaga() {
    yield all(sagasList.map(saga => call(saga)));
}
