import { combineReducers } from 'redux';
import temperatureUnitReducer from '../model/reducers/temperatureUnitReducer';

const rootReducer = combineReducers({
    temperatureUnit: temperatureUnitReducer,
});

export default rootReducer;
